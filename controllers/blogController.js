const { Blog, User } = require('../models/index')
const {Op} =require('sequelize')
exports.index = async (req, res, next) => {
    const blogs = await Blog.findAll({
        include:[
            {
                model:User,
                as:'user',
                attributes:['name','email'],
            }
        ]
    })
    
    res.status(200).json({
        message: blogs
    })
}