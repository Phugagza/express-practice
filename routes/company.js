const express = require('express');
const router = express.Router();
const companyController = require('../controllers/companyController')

/* GET users listing. */
router.get('/', companyController.index);
router.post('/readFile', companyController.readFile);

module.exports = router;
