const config =require('./index')

module.exports ={

  "development": {
    "username": config.DB_USERNAME,
    "password": config.DB_PASSWORD,
    "database": config.DB_NAME,
    "host": config.DB_HOST,
    "port":config.DB_PORT,
    "dialect": config.DB_DIALECT
  },
  "test": {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
  "production": {
    "username": config.DB_PRO_USERNAME,
    "password": config.DB_PRO_PASSWORD,
    "database": config.DB_PRO_NAME,
    "host": config.DB_PRO_HOST,
    "dialect": config.DB_PRO_DIALECT
  }

}