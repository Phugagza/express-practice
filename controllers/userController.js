const { User,Blog } = require('../models/index')
const { Op } = require('sequelize')
const bcrypt = require('bcrypt')

exports.index = async (req, res, next) => {
    
    // const users = await models.User.findAll();

    // const users = await models.User.findAll({
    //     attributes : ['id','name','email'],
    //     order:[['id','desc'],['email','asc']]
    // });

    // const users = await models.User.findAll({
    //     attributes : {exclude:['password']},
    //     order:[['id','desc']]
    // });

    // const users = await models.User.findAll({
    //     attributes : {exclude:['password']},
    //     where:{
    //         id:1
    //     },
    //     order:[['id','desc']]
    // });

    // const users = await models.User.findAll({
    //     attributes : ['id',['name','firstname'],'email'],
    //     where:{
    //         id: {
    //             [Op.eq]: 2
    //           }
    //     },
    //     order:[['id','desc']]
    // });

    // const sql = 'select * from users'
    // const users = await models.sequelize.query(sql,{
    //     type: models.sequelize.QueryTypes.SELECT
    // })

    //soft selete
    // let users = await models.User.findByPk(1)
    // users.destroy()

    //restore soft delete
    // let users = await models.User.findByPk(1 ,{ paranoid: false })
    // users.restore()

    //select with join
    const users = await User.findAll({
        attributes: { exclude: ['password', 'deletedAt'] },
        include: [
            {
                model: Blog,
                as: 'blogs',
                attributes: ['title', 'id'],
            }
        ],
        order: [
            ['id', 'asc'],
            ['blogs', 'id', 'desc']
        ]

    })

    res.status(200).json({
        data: users
    })
}

exports.show = async (req, res, next) => {
    try {
        const { id } = req.params

        const user = await User.findByPk(id, {
            attributes: ['id', ['name', 'firstname'], 'email'],
        })

        if (!user) {
            const err = new Error('not found');
            err.statuscode = 404;
            throw err;
        }

        res.status(200).json({
            data: user
        })
    } catch (err) {
        res.status(err.statuscode).json({
            data: err.message
        })
    }
}

exports.insert = async (req, res, next) => {
    try {
        const { name, email, password } = req.body;
        const exitUser = await User.findOne({
            where: {
                email: {
                    [Op.eq]: email
                }
            }
        })

        if (exitUser) {
            const err = new Error('มีผู้ใช้ในระบบแล้ว');
            err.statuscode = 400;
            throw err;
        }

        const salt = await bcrypt.genSalt(8);
        const passwordHash = await bcrypt.hash(password, salt)


        let user = await new User()
        user.name = name
        user.email = email
        user.password = passwordHash
        await user.save();

        res.status(201).json({
            data: user
        })
    } catch (err) {
        res.status(err.statuscode).json({
            data: err.message
        })
    }
}

exports.update = async (req, res, next) => {
    try {
        const { id, name, email, password } = req.body;

        if (req.params.id !== id) {
            const err = new Error('ไม่พบผู้ใช้งาน');
            err.statuscode = 400;
            throw err;
        }

        const salt = await bcrypt.genSalt(8);
        const passwordHash = await bcrypt.hash(password, salt)

        let user = await User.update({
            name: name,
            email: email,
            password: passwordHash
        }, {
            where: {
                id: {
                    [Op.eq]: id
                }
            }
        })

        res.status(200).json({
            data: user
        })
    } catch (err) {
        res.status(err.statuscode).json({
            data: err.message
        })
    }
}

exports.delete = async (req, res, next) => {
    try {
        const { id } = req.params

        const user = await User.findByPk(id)

        if (!user) {
            const err = new Error('ไม่พบผู้ใช้งาน');
            err.statuscode = 404;
            throw err;
        }

        user.destroy();

        res.status(200).json({
            data: {
                message: "ลบเรียบร้อย"
            }
        })
    } catch (err) {
        res.status(err.statuscode).json({
            data: err.message
        })
    }
}