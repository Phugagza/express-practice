const env = require('dotenv')
env.config();

module.exports={
    PORT:process.env.PORT,
    NODE_ENV:process.env.NODE_ENV,
    DB_USERNAME:process.env.DB_USERNAME,
    DB_PASSWORD:process.env.DB_PASSWORD,
    DB_NAME:process.env.DB_NAME,
    DB_HOST:process.env.DB_HOST,
    DB_PORT:process.env.DB_PORT,
    DB_DIALECT:process.env.DB_DIALECT,

    DB_PRO_USERNAME:process.env.DB_PRO_USERNAME,
    DB_PRO_PASSWORD:process.env.DB_PRO_PASSWORD,
    DB_PRO_NAME:process.env.DB_PRO_NAME,
    DB_PRO_HOST:process.env.DB_PRO_HOST,
    DB_PRO_PORT:process.env.DB_PRO_PORT,
    DB_PRO_DIALECT:process.env.DB_PRO_DIALECT,
}