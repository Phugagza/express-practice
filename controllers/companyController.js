const { Company } = require('../models/index')
const fs = require('fs')
const  Formidable  = require('formidable');
const Excel = require('exceljs')

exports.index = async (req, res, next) => {
    const companys = await Company.findAll()

    res.status(200).json({
        data: companys
    })
}

exports.readFile = async (req,res,next) =>{
    
    const form = new Formidable.IncomingForm();
    form.parse(req,async(err,fields, files)=>{
        // console.log(files['excelFile'].path)
        const workbook = new Excel.Workbook();
         await workbook.xlsx.readFile(files['excelFile'].path); 
        const ws = await workbook.getWorksheet("รายละเอียดการลงทะเบียน")
        const cell =  await ws.getColumn('E')
        
        
        
        console.log(cell.values)
        res.status(200).json({
            message: 'done'
        })
    })
    
    
}